<?php require "connection.php"?>
<?php
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add members</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Add members</h1>
    <div class="container_form">
        <form method="POST">
            <label>First name</label>
            <input type="text" name="firstName">
            <label>Last name</label>
            <input type="text" name="lastName">
            <button type="submit" name="submit" value="submit">Submit</button>
        </form>

    <?php
     if (isset($_POST['firstName']) && isset($_POST['lastName'])) {
        $firstname = $_POST['firstName'];
        $lastname = $_POST['lastName'];
        $sql = 'INSERT INTO team (first_name, last_name) VALUES ("' . $firstname . '", "' . $lastname . '")';
        echo $sql;
        $result = $conn->query($sql);
        if ($result) {
            header("Location:index.php");
        } else {
            echo '<p class="error">Une erreur est survenue</p>';
        }
    }
      ?>

    </div>
</body>
</html>