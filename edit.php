
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TODO List</title>
    <link href="style.css" rel="stylesheet">
</head>

<body>
    
<h1>Edit task</h1>
    <div class="main_container">
        
        <?php
        require 'database.php';
        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $res = $conn->query("SELECT * FROM todo_list WHERE id = " . $id);
            $row = $res->fetch_assoc();
            if (isset($row['task'])) {
        ?>
        <div class="edit_form">
            <form method="POST">
                <input type="text" value="<?php echo $row['task']; ?>" name="task">
                <input type="submit" value="Update">
            </form>
        </div>
        <?php } ?>
        <?php } ?>

        <?php
        if (isset($_POST['task'])) {
            $task = $_POST['task'];
            $requete = 'UPDATE todo_list SET task="' . $task . '" WHERE id = ' . $id;
            $resultat = $conn->query($requete);
            if ($resultat) {
                header("Location:index.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>
    </div>
</body>

</html>
