
<?php require "connection.php"?>
<?php
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Assign a task</h1>
<div class="main_container">
    <form method="post">
        <div class="filters">
            <div class="task_list">
                <?php
                    $request = "SELECT * FROM todo_list";
                    $result_task = $conn->query($request);?>
                <select name = "task">
                    <option hidden>Tasks</option>
                        <?php foreach ($result_task as $task_list){?>
                    <option value = "<?php echo $task_list['id'];?>">
                        <?php echo $task_list['task'];?>
                    </option>
                        <?php } ?>
                </select>
            </div>
            <div class="member_list">
                <?php
                    $request = "SELECT * FROM team";
                    $result_members= $conn->query($request);?>
                <select name = "member">
                    <option hidden>Members</option>
                        <?php foreach ($result_members as $members_list){?>
                    <option value="<?php echo $members_list['id'];?>">
                        <?php echo $members_list['first_name'];?>
                        <?php echo $members_list['last_name'];?>
                    </option>
                        <?php } ?>
                </select>
            </div>
            <input type="submit" name="hi" value="ok">
        </div>
    </form>

        <?php
        if (isset($_POST['task']) && isset($_POST['member'])) {
        $taskId = $_POST['task'];
        $memberId = $_POST['member'];
        $sql = 'INSERT INTO asign (id_task, id_team) VALUES ("' . ((INT)$taskId) . '", "' . ((INT)$memberId) . '")';
        echo $sql;
        $result = $conn->query($sql);
        if ($result) {
            header("Location:index.php");
        } else {
            echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>

 </div>       
   
    
</body>
</html>

