<?php require "connection.php"?>
<?php
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>TO DO</title>
      <link rel="stylesheet" href="style.css">
  </head>
  
<body>
    <h1>To Do List</h1>
    <div class="main_container">
      <div class="list_container">
        <table>
            <tr>
              <th>ID</th>
              <th>Task</th>
              <th>Members</th>
              <th></th>
            </tr>
        <?php
          $sql = "SELECT * FROM todo_list";
          $result = $conn->query($sql); 
          foreach ($result as $task){ ?>
            <tr>
              <td>
                <?php echo $task['id'];?>
              </td>
              <td>
                <?php echo $task['task'];?>
              </td>
              <?php 
          $joinTable = "SELECT team.first_name, team.last_name, todo_list.task FROM team 
            JOIN asign ON team.id = asign.id_team 
            JOIN todo_list ON todo_list.id = asign.id_task 
            WHERE todo_list.id = " . $task['id'];
          $resJoin = $conn->query($joinTable);
            ?>
            <td>
            <?php foreach ($resJoin as $member) { ?>
                <p><?php echo $member['first_name'] . ' ' . $member['last_name'];?></p>
            <?php } ?>
            </td>
              <td>
                <a href = edit.php?id=<?php echo $task['id'];?>><button>Edit</button></a>
                <a href = delete.php?id=<?php echo $task['id'];?>><button>Delete</button></a>
              </td>
            </tr>
            <?php } ?>
        </table>
          <div class="button-container">
            <a href="user_story.php"><button type="button">Add task</button></a>
            <a href="add_member.php"><button type="button">Add members</button></a>
            <a href="assign_member.php"><button type="button">Assign member to a task</button></a>
         </div>
      </div>
    </div>  
  </body>
</html>

